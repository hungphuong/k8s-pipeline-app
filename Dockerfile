FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /app/aspnetapp

# copy csproj and restore as distinct layers
# COPY *.sln .
COPY . .
RUN dotnet restore

# copy everything else and build app
RUN dotnet publish -c Release -o out


FROM microsoft/dotnet:2.1-aspnetcore-runtime AS runtime
WORKDIR /app
COPY --from=build /app/aspnetapp/out ./
ENTRYPOINT ["dotnet", "k8s-pipeline-app.dll"]
